# Prova estágio Gama

Prova do ProjetoGama feito para a vaga de estágio mobile.

## Exercício 1

Na pasta do exercício 1 contém o script do banco de dados na linguagem sql.
Foi criado manualmente a tabela conforme solicitado pelo exercício e feita a correção do script do exemplo. O teste do script foi feito no site MyCompiler.

## Exercício 2

Na pasta do exercício 2 contém o script das funções solicitadas feitos em linguagem Java.
O projeto foi feito e testado via VScode. O projeto tem uma lista pré-definida onde todas as validações são feitas nas funções específicas.
O projeto possui dois scripts em Java onde um script possui todas as funções e o outro apenas chama as funções.

## Exercício 3

Na pasta exercício 3 contém o script da função solicitada feita em linguagem C++.
O arquivo esta em formato .txt pois foi testado no site https://www.programiz.com/cpp-programming/online-compiler/.