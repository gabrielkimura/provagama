# Java Project

Projeto Java com tratamento de lista de produtos.

## Pré-requisitos

[Extensão Java](https://javadl.oracle.com/webapps/download/AutoDL?BundleId=245807_df5ad55fdd604472a86a45a217032c7d)
[Vscode](https://code.visualstudio.com/)

## Executando

1. Instalar os pré-requisitos.
2. Abrir a pasta *Exercicio2* no VScode.
3. Baixar as enxtensões Debugger for Java e Language Support for Java(TM) by Red Hat.
4. Clicar no botão executar e depurar.
5. Irá aparecer um terminal logo abaixo com o log da execução.
