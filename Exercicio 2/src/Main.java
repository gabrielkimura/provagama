import java.util.*;
import java.text.*;

public class Main{

    public static void main(String[] args) throws ParseException {

        SimpleDateFormat sdformat = new
        SimpleDateFormat("yyyy-MM-dd");

        List<Estoque> x = new ArrayList<>();
        x.add(new Estoque(1,1,100,250,true,sdformat.parse("2022-02-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(2,2,150,50,true,sdformat.parse("2020-07-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(3,3,0,200,false,sdformat.parse("2021-09-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(4,5,100,100,true,sdformat.parse("2021-01-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(5,6,300,138,true,sdformat.parse("2021-02-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(6,7,500,20,true,sdformat.parse("2021-03-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(7,9,365,20,true,sdformat.parse("2021-04-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(8,8,80,20,true,sdformat.parse("2021-05-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(9,10,52,20,true,sdformat.parse("2019-12-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(10,11,5452,20,true,sdformat.parse("2021-02-14"),sdformat.parse("2022-02-14")));
        x.add(new Estoque(11,4,562,20,true,sdformat.parse("2020-08-14"),sdformat.parse("2022-02-14")));

        System.out.println("Tópico 1:");
        List<Estoque> lista_ativos = new ArrayList<>();
        lista_ativos = x.get(0).ListaAtivos(x);

        for(int i =0; i<lista_ativos.size(); i++){
        System.out.println("Produto "+lista_ativos.get(i).CodigoProduto+" esta ativo e incluso a mais de 1 ano");
        }
        
        System.out.println("\nTópico 2:");
        List<Estoque> lista_quantidade = new ArrayList<>();
        List<Estoque> lista_semEstoque = new ArrayList<>();
        lista_semEstoque = x.get(0).semEstoque(x);
        lista_quantidade = x.get(0).QtdMin(x);

        for(int i =0; i<lista_quantidade.size(); i++){
            System.out.println("Produto " + lista_quantidade.get(i).CodigoProduto + " com minimo abaixo de 200");
        }

        for(int i =0; i<lista_semEstoque.size(); i++){
            System.out.println("Produto " + lista_semEstoque.get(i).CodigoProduto + " esta sem estoque");
        }

        System.out.println("\nTópico 3:");
        Collections.sort(x);
        for(int i = 0; i < 10; i++)
           System.out.println("Produto "+x.get(i).CodigoProduto+" foi incluso na data de "+x.get(i).Inclusao);
      
        System.out.println("\nTópico 4:");
        Collections.sort(x,Comparator.comparing(Estoque::getQuantidade));
        for(int i = 10; i > 0; i--)
           System.out.println("Produto "+x.get(i).CodigoProduto+" quantidade "+x.get(i).Quantidade);
    }
}