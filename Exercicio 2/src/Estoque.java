import java.util.*;

public class Estoque implements Comparable<Estoque>{
    public int Codigo;
    public int CodigoProduto;
    public int Quantidade;
    public int Minimo;
    public boolean Ativo;
    public Date Inclusao;
    public Date Alteracao;
    
    Estoque(int Codigo, int CodigoProduto, int Quantidade, int Minimo, boolean Ativo, Date dt_Inclusao, Date Alteracao){
        super();
        this.Codigo = Codigo;
        this.CodigoProduto = CodigoProduto;
        this.Quantidade = Quantidade;
        this.Minimo = Minimo;
        this.Ativo = Ativo;
        this.Inclusao = dt_Inclusao;
        this.Alteracao = Alteracao; 
    }

    public int getCodigo(){
        return this.Codigo;
    }

    public int getQuantidade(){
        return this.Quantidade;
    }

    public Date getInclusao(){
        return this.Inclusao;
    }

    public int getMinimo(){
        return this.Minimo;
    }

    public int getCodigoProduto(){
        return this.CodigoProduto;
    }

    public List<Estoque> ListaAtivos(List<Estoque> x){
        List<Estoque> myList = new LinkedList<Estoque>();

        for(int i = 0; i<x.size(); i++)
            if(x.get(i).Ativo)
                if((converteDias(new Date()) - converteDias(x.get(i).Inclusao)) > 365)
                    myList.add(x.get(i));
        return myList;             
    }

    public List<Estoque> QtdMin(List<Estoque> y){
        List<Estoque> lista = new LinkedList<Estoque>();
        
        for(int i = 0; i<y.size(); i++)
            if(y.get(i).Minimo < 200)
                lista.add(y.get(i));
        return lista;     
    }

    public List<Estoque> semEstoque(List<Estoque> y){
        List<Estoque> lista = new LinkedList<Estoque>();
        
        for(int i = 0; i<y.size(); i++)
            if(y.get(i).Quantidade <= 0)
                lista.add(y.get(i));
        return lista;     
    }

    public int compareTo(Estoque estoque){
        if(this.Inclusao.before(estoque.Inclusao))
            return -1;

        else if (this.Inclusao.after(estoque.Inclusao))
            return 1;

        if(this.Inclusao.equals(estoque.Inclusao)){
            if(this.CodigoProduto < estoque.CodigoProduto)
            return -1;
        else if (this.CodigoProduto > estoque.CodigoProduto)
            return 1;
        }
        return 0;
    }

    public double converteDias(Date x){return (x.getTime()*0.000000000031688738506811) * 365;}
}